<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211219163840 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE photo_galerie DROP FOREIGN KEY FK_AB9D021DA76ED395');
        $this->addSql('DROP INDEX IDX_AB9D021DA76ED395 ON photo_galerie');
        $this->addSql('ALTER TABLE photo_galerie DROP user_id, CHANGE created_at date DATETIME NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE photo_galerie ADD user_id INT NOT NULL, CHANGE date created_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE photo_galerie ADD CONSTRAINT FK_AB9D021DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_AB9D021DA76ED395 ON photo_galerie (user_id)');
    }
}
