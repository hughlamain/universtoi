<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211017230231 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE photo_galerie ADD galerie_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE photo_galerie ADD CONSTRAINT FK_AB9D021D825396CB FOREIGN KEY (galerie_id) REFERENCES galerie (id)');
        $this->addSql('CREATE INDEX IDX_AB9D021D825396CB ON photo_galerie (galerie_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE photo_galerie DROP FOREIGN KEY FK_AB9D021D825396CB');
        $this->addSql('DROP INDEX IDX_AB9D021D825396CB ON photo_galerie');
        $this->addSql('ALTER TABLE photo_galerie DROP galerie_id');
    }
}
