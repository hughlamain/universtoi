<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Commentaire;
use App\Entity\Questionnaire;
use App\Entity\Article;
use DateTime;

class CommentaireUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $commentaire = new Commentaire();
        $questionnaire = new Questionnaire();
        $article = new Article();
        $datetime = new DateTime();


        $commentaire->setAuteur('auteur')
                    ->setCreateAt($datetime)
                    ->setContenu('contenu')
                    ->setEmail('email')
                    ->setQuestionnaire($questionnaire)
                    ->setArticle($article);

        $this->assertTrue($commentaire->getAuteur() === 'auteur');
        $this->assertTrue($commentaire->getCreateAt() === $datetime);
        $this->assertTrue($commentaire->getContenu() === 'contenu');
        $this->assertTrue($commentaire->getEmail() === 'email');
        $this->assertTrue($commentaire->getQuestionnaire() === $questionnaire);
        $this->assertTrue($commentaire->getArticle() === $article);
    }

    public function testIsFalse()
    {
        $commentaire = new Commentaire();
        $questionnaire = new Questionnaire();
        $article = new Article();
        $datetime = new DateTime();

        $commentaire->setAuteur('auteur')
                    ->setCreateAt($datetime)
                    ->setContenu('contenu')
                    ->setEmail('email')
                    ->setQuestionnaire($questionnaire)
                    ->setArticle($article);

        $this->assertFalse($commentaire->getAuteur() === 'false');
        $this->assertFalse($commentaire->getCreateAt() === new DateTime());
        $this->assertFalse($commentaire->getContenu() === 'false');
        $this->assertFalse($commentaire->getEmail() === 'false');
        $this->assertFalse($commentaire->getQuestionnaire() === new Questionnaire);
        $this->assertFalse($commentaire->getArticle() === new Article);
    }

    public function testIsEmpty()
    {
        $commentaire = new Commentaire();

            $this->assertEmpty($commentaire->getAuteur());
            $this->assertEmpty($commentaire->getCreateAt() === new DateTime());
            $this->assertEmpty($commentaire->getContenu());
            $this->assertEmpty($commentaire->getEmail());
            $this->assertEmpty($commentaire->getQuestionnaire());
            $this->assertEmpty($commentaire->getArticle());

    }
}
