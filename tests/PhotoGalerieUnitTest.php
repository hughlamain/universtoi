<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\PhotoGalerie;
use App\Entity\Galerie;
use App\Entity\Categorie;
use DateTime;


class PhotoGalerieUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $photoGalerie = new PhotoGalerie();
        $datetime = new DateTime();
        $galerie = new Galerie();
        $categorie = new Categorie;

        $photoGalerie->setNom('nom')
                     ->setDescription('description')
                     ->setFichier('fichier')
                     ->setSlug('slug')
                     ->setGalerie($galerie)
                     ->addCategorie($categorie)
                     ->setDate($datetime);


        $this->assertTrue($photoGalerie->getNom() === 'nom');
        $this->assertTrue($photoGalerie->getDescription() ==='description');
        $this->assertTrue($photoGalerie->getDate() === $datetime);
        $this->assertTrue($photoGalerie->getFichier() === 'fichier');
        $this->assertContains($categorie, $photoGalerie->getCategorie());
        $this->assertTrue($photoGalerie->getSlug() === 'slug');
        $this->assertTrue($photoGalerie->getGalerie() === $galerie);
    }

    public function testIsFalse()
    {
        $photoGalerie = new PhotoGalerie();
        $galerie = new Galerie();
        $datetime = new DateTime();
        $categorie = new Categorie;

        $photoGalerie->setNom('nom')
                     ->setDescription('description')
                     ->setFichier('fichier')
                     ->setSlug('slug')
                     ->setGalerie($galerie)
                     ->addCategorie($categorie)
                     ->setDate($datetime);

                     $this->assertFalse($photoGalerie->getNom() === 'false');
                     $this->assertFalse($photoGalerie->getDate() === new DateTime());
                     $this->assertFalse($photoGalerie->getDescription() ==='false');
                     $this->assertFalse($photoGalerie->getFichier() === 'false');
                     $this->assertFalse($photoGalerie->getSlug() === 'false');
                     $this->assertNotContains(new Categorie(), $photoGalerie->getCategorie());
                     $this->assertFalse($photoGalerie->getGalerie() === new Galerie);
    }

    public function testIsEmpty()
    {
        $photoGalerie = new PhotoGalerie();

            $this->assertEmpty($photoGalerie->getNom());
            $this->assertEmpty($photoGalerie->getDate());
            $this->assertEmpty($photoGalerie->getDescription());
            $this->assertEmpty($photoGalerie->getFichier());
            $this->assertEmpty($photoGalerie->getSlug());
            $this->assertEmpty($photoGalerie->getCategorie());
            $this->assertEmpty($photoGalerie->getGalerie());

    }
}
