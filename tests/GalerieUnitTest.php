<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Galerie;
use App\Entity\PhotoGalerie;


class GalerieUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $galerie = new Galerie();

        $galerie->setNom('nom')
                 ->setDescription('description')
                 ->setSlug('slug');

            $this->assertTrue($galerie->getNom() === 'nom');
            $this->assertTrue($galerie->getDescription() === 'description');
            $this->assertTrue($galerie->getSlug() === 'slug');
    }

    public function testIsFalse()
    {
        $galerie = new Galerie();

        $galerie->setNom('nom')
                ->setDescription('description')
                ->setSlug('slug');

            $this->assertFalse($galerie->getNom() === 'false');
            $this->assertFalse($galerie->getDescription() === 'false');
            $this->assertFalse($galerie->getSlug() === 'false');
    }

    public function testIsEmpty()
    {
        $galerie = new Galerie();
            $this->assertEmpty($galerie->getNom());
            $this->assertEmpty($galerie->getDescription());
            $this->assertEmpty($galerie->getSlug());
    }
}
