<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Article;
use DateTime;
use App\Entity\User;

class ArticleUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $article = new Article();
        $datetime = new DateTime();
        $user = new User();

        $article->setTitre('titre')
                ->setDate($datetime)
                ->setContenu('contenu')
                ->setSlug('slug')
                ->setUser($user);

        $this->assertTrue($article->getTitre() === 'titre');
        $this->assertTrue($article->getDate() === $datetime);
        $this->assertTrue($article->getContenu() === 'contenu');
        $this->assertTrue($article->getSlug() === 'slug');
        $this->assertTrue($article->getUser() === $user);
        
    }

    public function testIsFalse()
    {
        $article = new Article();
        $datetime = new DateTime();
        $user = new User();

        $article->setTitre('titre')
                ->setDate($datetime)
                ->setContenu('contenu')
                ->setSlug('slug')
                ->setUser($user);

        $this->assertFalse($article->getTitre() === 'false');
        $this->assertFalse($article->getDate() === new DateTime());
        $this->assertFalse($article->getContenu() === 'false');
        $this->assertFalse($article->getSlug() === 'false');
        $this->assertFalse($article->getUser() === new User());
    }

    public function testIsEmpty()
    {
        $article = new Article();
        
            $this->assertEmpty($article->getTitre());
            $this->assertEmpty($article->getDate());
            $this->assertEmpty($article->getContenu());
            $this->assertEmpty($article->getSlug());
            $this->assertEmpty($article->getUser());
            
    }
}
