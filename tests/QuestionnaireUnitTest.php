<?php

namespace App\Tests;

use App\Entity\Questionnaire;
use App\Entity\Categorie;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

use function PHPUnit\Framework\assertEmpty;

class QuestionnaireUnitTest extends TestCase
{
    public function testIsTrue()
    {
    $questionnaire = new Questionnaire();
    $datetime = new DateTime();
    $categorie = new Categorie;
    $user = new User();

        $questionnaire->setNom('nom')
                      ->setDate($datetime)
                      ->setQuestion('question')
                      ->setType('type')
                      ->addCategorie($categorie)
                      ->setSlug('slug')
                      ->setUser($user);

        $this->assertTrue($questionnaire->getNom() === 'nom');
        $this->assertTrue($questionnaire->getDate() === $datetime);
        $this->assertTrue($questionnaire->getQuestion() === 'question');
        $this->assertTrue($questionnaire->getType() === 'type');
        $this->assertContains($categorie, $questionnaire->getCategorie());
        $this->assertTrue($questionnaire->getSlug() === 'slug');
        $this->assertTrue($questionnaire->getUser() === $user);
    }

    public function testIsFalse()
    {
        $questionnaire = new Questionnaire();
        $datetime = new DateTime();
        $categorie = new Categorie;
        $user = new User();
      
        $questionnaire->setNom('nom')
                      ->setDate($datetime)
                      ->setQuestion('question')
                      ->setType('type')
                      ->addCategorie($categorie)
                      ->setSlug('slug')
                      ->setUser($user);

            $this->assertFalse($questionnaire->getNom() === 'false');
            $this->assertFalse($questionnaire->getDate() === new DateTime());
            $this->assertFalse($questionnaire->getQuestion() === 'false');
            $this->assertFalse($questionnaire->getType() === 'false');
            $this->assertNotContains(new Categorie(), $questionnaire->getCategorie());
            $this->assertFalse($questionnaire->getUser() === new User());
    }

    public function testIsEmpty()
    {
        $questionnaire = new Questionnaire();
        
            $this->assertEmpty($questionnaire->getNom());
            $this->assertEmpty($questionnaire->getDate());
            $this->assertEmpty($questionnaire->getQuestion());
            $this->assertEmpty($questionnaire->getType());
            $this->assertEmpty($questionnaire->getCategorie());
            $this->assertEmpty($questionnaire->getSlug());
            $this->assertEmpty($questionnaire->getUser());
    }
}
