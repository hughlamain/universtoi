<?php

namespace App\Controller;

use App\Repository\QuestionnaireRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class QuestionnaireController extends AbstractController
{
    /**
     * @Route("/questionnaires", name="questionnaires")
     */
    public function questionnaires(
        QuestionnaireRepository $questionnaireRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
            $data = $questionnaireRepository->findAll();

            $questionnaires = $paginator->paginate(
                $data,
                $request->query->getInt('page', 1),
                6
            );

        return $this->render('questionnaire/questionnaires.html.twig', [
            'questionnaires' => $questionnaires,
        ]);
    }
}
