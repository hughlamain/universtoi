<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PhotoGalerieController extends AbstractController
{
    /**
     * @Route("/photo/galerie", name="photo_galerie")
     */
    public function index(): Response
    {
        return $this->render('photo_galerie/index.html.twig', [
            'controller_name' => 'PhotoGalerieController',
        ]);
    }
}
