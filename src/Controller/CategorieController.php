<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Repository\PhotoGalerieRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CategorieController extends AbstractController
{

    /**
     * @Route("galerie/categorie/{slug}", name="categorie_categorie")
     */
    public function categorie(
        Categorie $categorie,
        PhotoGalerieRepository $photoGalerieRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $data = $photoGalerieRepository->findAll();

            $photos = $paginator->paginate(
                $data,
                $request->query->getInt('page', 1),
                6
            );

        return $this->render('galerie/categorie.html.twig', [
            'categorie' => $categorie,
            'photos' => $photos,
        ]);
    }
}
