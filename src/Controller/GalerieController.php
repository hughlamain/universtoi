<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Repository\CategorieRepository;
use App\Repository\PhotoGalerieRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GalerieController extends AbstractController
{
    /**
     * @Route("/galerie", name="galerie")
     */
    public function categories(
        CategorieRepository $categorieRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $data = $categorieRepository->findAll();

        $categories = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            3
        );

        return $this->render('galerie/galerie.html.twig', [
            'categories' => $categories,
        ]);
    }

    // /**
    //  * @Route("/categorie/{slug}", name="categorie_categorie")
    //  */
    // public function categorie(Categorie $categorie, PhotoGalerieRepository $photoGalerieRepository): Response
    // {
    //     $photos = $photoGalerieRepository->findAllPhotoGalerie($categorie);

    //     return $this->render('galerie/categorie.html.twig', [
    //         'categorie' => $categorie,
    //         'photos' => $photos,
    //     ]);
    // }
}
