<?php

namespace App\Repository;

use App\Entity\Categorie;
use App\Entity\PhotoGalerie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PhotoGalerie|null find($id, $lockMode = null, $lockVersion = null)
 * @method PhotoGalerie|null findOneBy(array $criteria, array $orderBy = null)
 * @method PhotoGalerie[]    findAll()
 * @method PhotoGalerie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhotoGalerieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PhotoGalerie::class);
    }


    /**
     * @return PhotoGalerie[] Return an array of PhotoGalerie objects
     */
    public function findAllPhotoGalerie(Categorie $categorie): array
    {
        return $this->createQueryBuilder('p')
            ->where(':categorie MEMBER OF p.categorie')
            ->setParameter('categorie', $categorie)
            ->getQuery()
            ->getResult()
            ;
    }
}
