<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Article;
use App\Entity\Categorie;
use App\Entity\PhotoGalerie;
use App\Entity\Questionnaire;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        //Utilisation de Faker
        $faker = Factory::create('fr_FR');

        //Creation d'un utilisateur
        $user = new User();

        $user->setEmail('user@test.com')
             ->setPrenom($faker->firstname())
             ->setNom($faker->lastName())
             ->setTelephone($faker->phoneNumber())
             ->setCommentaire($faker->text())
             ->setRoles(['ROLE_ADMIN']);

        $password = $this->encoder->hashPassword($user, 'password');
        $user->setPassword($password);

        $manager->persist($user);

        //Creation de 10 articles
        for ($i=0; $i < 10; $i++) {
            $article = new Article();

            $article->setTitre($faker->words(3, true))
                    ->setDate($faker->dateTimeBetween('-6 month', 'now'))
                    ->setContenu($faker->text(350))
                    ->setImage($faker->words(10, true))
                    ->setSlug($faker->slug(3))
                    ->setUser($user);

            $manager->persist($article);
        }

        //Creation de 5 categories
        for ($k=0; $k < 5; $k++) {
            $categorie = new Categorie();

            $categorie->setNom($faker->words(3, true))
                      ->setDescription($faker->words(10, true))
                      ->setSlug($faker->slug(3));

            $manager->persist($categorie);


            //Creation de 2 questionnaires/categorie
            for ($j=0; $j < 2; $j++) {
                $questionnaire = new Questionnaire();

                $questionnaire->setNom($faker->words(4, true))
                              ->setDate($faker->dateTimeBetween('-6 month', 'now'))
                              ->setQuestion($faker->words(10, true))
                              ->setImage($faker->words(10, true))
                              ->setType($faker->word(4, true))
                              ->setSlug($faker->slug())
                              ->addCategorie($categorie)
                              ->setUser($user);

                $manager->persist($questionnaire);
            }

            //Creation de 10 photos/categorie
            for ($l=0; $l < 10; $l++) {
                $photoGalerie = new PhotoGalerie();

                $photoGalerie->setNom($faker->words(4, true))
                             ->setDescription($faker->words(10, true))
                             ->setFichier($faker->words(10, true))
                             ->setDate($faker->dateTimeBetween('-6 month', 'now'))
                             ->setSlug($faker->slug())
                             ->addCategorie($categorie);

                $manager->persist($photoGalerie);
            }
        }

        $manager->flush();
    }
}
