<?php

namespace App\Entity;

use App\Repository\CategorieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategorieRepository::class)
 */
class Categorie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\ManyToMany(targetEntity=Questionnaire::class, mappedBy="categorie")
     */
    private $questionnaires;

    /**
     * @ORM\ManyToMany(targetEntity=PhotoGalerie::class, mappedBy="categorie")
     */
    private $photoGaleries;

    public function __construct()
    {
        $this->questionnaires = new ArrayCollection();
        $this->photoGaleries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|Questionnaire[]
     */
    public function getQuestionnaires(): Collection
    {
        return $this->questionnaires;
    }

    public function addQuestionnaire(Questionnaire $questionnaire): self
    {
        if (!$this->questionnaires->contains($questionnaire)) {
            $this->questionnaires[] = $questionnaire;
            $questionnaire->addCategorie($this);
        }

        return $this;
    }

    public function removeQuestionnaire(Questionnaire $questionnaire): self
    {
        if ($this->questionnaires->removeElement($questionnaire)) {
            $questionnaire->removeCategorie($this);
        }

        return $this;
    }

    /**
     * @return Collection|PhotoGalerie[]
     */
    public function getPhotoGaleries(): Collection
    {
        return $this->photoGaleries;
    }

    public function addPhotoGalery(PhotoGalerie $photoGalery): self
    {
        if (!$this->photoGaleries->contains($photoGalery)) {
            $this->photoGaleries[] = $photoGalery;
            $photoGalery->addCategorie($this);
        }

        return $this;
    }

    public function removePhotoGalery(PhotoGalerie $photoGalery): self
    {
        if ($this->photoGaleries->removeElement($photoGalery)) {
            $photoGalery->removeCategorie($this);
        }

        return $this;
    }
}
